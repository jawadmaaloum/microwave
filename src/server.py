import routes
import cherrypy
from microwave_controller import MicrowaveController

def start_service():
    m_cont = MicrowaveController()
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    dispatcher.connect( 'get_time_key',
                        '/microwave/:food/:start_temp/:wattage',
                        controller=m_cont,
                        action = 'GET_TIME_KEY',
                        conditions=dict(method=['GET']) )

    dispatcher.connect( 'get_review_all',
                        '/microwave/reviews',
                        controller=m_cont,
                        action = 'GET_REVIEW_ALL',
                        conditions=dict(method=['GET']) )

    dispatcher.connect( 'get_review_key',
                        '/microwave/reviews/:food',
                        controller=m_cont,
                        action = 'GET_REVIEW_KEY',
                        conditions=dict(method=['GET']) )

    dispatcher.connect( 'post_review_index',
                        '/microwave/reviews',
                        controller=m_cont,
                        action = 'POST_REVIEW_INDEX',
                        conditions=dict(method=['POST']) )

    conf = {
        'global' : {
            'server.socket_host': 'localhost',
            'server.socket_port': 51048,
        },
        '/' : {
            'request.dispatch': dispatcher,
            'tools.CORS.on': True
        }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# class for CORS
class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

# function for CORS
def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

if __name__ == "__main__":
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()
