console.log('entered result.js');

var food = sessionStorage.getItem("food");
console.log("loaded food: " + food);
updateResultWithResponse();

function updateResultWithResponse(){
  var response_json = JSON.parse(sessionStorage.getItem("responseText"));

  var label0 = document.getElementById("query_response");
  var label1 = document.getElementById("response_label");

  var thumbsUpButton = document.getElementById('thumbs-up-button');
  thumbsUpButton.onmouseup = sendGoodReview;

  var thumbsDownButton = document.getElementById('thumbs-down-button');
  thumbsDownButton.onmouseup = sendBadReview;

  var reviews = document.getElementById('review_label');


  if(response_json['result'] == 'error'){
    label0.innerHTML = 'We don\'t know that food!';
    thumbsDownButton.style.display = 'none';
    thumbsUpButton.style.display = 'none';
    return;
  }

  reviews.innerHTML = "Leave a rating below!";

  var food = sessionStorage.getItem("food");
  var time = parseInt(response_json['time']);
  console.log("seconds = " + time);

  // convert to min and secs
  var minutes = ~~(time / 60);
  var seconds = (time % 60).toLocaleString(undefined, {minimumIntegerDigits: 2});

  label0.innerHTML = "How long does it take to microwave " + food + "?";
  label1.innerHTML =  minutes + ":" + seconds;
}

function sendGoodReview() { sendReview(food, 'good'); }
function sendBadReview() { sendReview(food, 'bad'); }

function sendReview(food, review) {
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:51048/microwave/reviews";
    body = JSON.stringify({"food": food, "review": review});
    console.log("made request to " + url + " with body: " + body);

    xhr.open("POST", url, true);
    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updateButtons();
    }
    xhr.onerror = function(e) {
        console.error(xhr.responseText);
    }
    xhr.send(body);
}

function updateButtons() {
    var thumbsUpButton = document.getElementById('thumbs-up-button');
    var thumbsDownButton = document.getElementById('thumbs-down-button');
    thumbsUpButton.style.display = "none"; // disable button
    thumbsDownButton.style.display = "none"; // disable button
    console.log('disabled buttons');
    getReviewResponse();
}

function getReviewResponse(){
  var xhr = new XMLHttpRequest();
  var url = "http://localhost:51048/microwave/reviews/" + food;

  xhr.open("GET", url, true);

  xhr.onload = function(e) {
      console.log(xhr.responseText);
      updateReviewWithResponse(xhr.responseText);
  }

  xhr.onerror = function(e) {
      console.error(xhr.responseText);
  }

  xhr.send(null);
}

function updateReviewWithResponse(responseText){
  var response_json = JSON.parse(responseText);
  var reviews = document.getElementById('review_label');

  if(response_json['result'] == 'error'){
    reviews.innerHTML = "Error";
    return;
  }

  var good = response_json['good'];
  var bad = response_json['bad'];
  var percent = (good / (good + bad)) * 100;
  reviews.innerHTML = (percent).toFixed(2) + "% of people recommend this time!";

}
