console.log('entered main.js')

var submitButton = document.getElementById('microwave-submit-button');
submitButton.addEventListener('click', getFormInfo);

var settingsButton = document.getElementById('microwave-settings-button');
settingsButton.addEventListener('click', toggleSettings);

function toggleSettings() {
    var forms = document.getElementById("settings-forms");
    if (forms.style.display === "none") {
        console.log('settings opened');
        forms.style.display = "block";
    } else {
        console.log('settings hidden');
        forms.style.display = "none";
    }
}

function toTempString(temp) {
    switch(temp) {
        case "Frozen":
            return "frozen";
        case "Refrigerated":
            return "refrigerated";
        case "Room Temp":
            return "room_temp";
    }
}

function getFormInfo() {
    console.log('entered getFormInfo');
    var food = document.getElementById('food-input').value;
    var temp = toTempString(document.getElementById('select-temp').value);
    var wattage = document.getElementById('select-wattage').value;
    console.log([food, temp, wattage].join(' '));
    makeNetworkCallToMicrowaveApi(food, temp, wattage);
}

function makeNetworkCallToMicrowaveApi(food, temp, wattage) {

    var xhr = new XMLHttpRequest();
    var url = "http://localhost:51048/microwave/" + food + "/" + temp + "/" + wattage;
    console.log('made request to ' + url);
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
        console.log(xhr.responseText);
        updatePageWithResponse(food, xhr.responseText);
    }

    xhr.onerror = function(e) {
        console.error(xhr.responseText);
    }

    xhr.send(null);
}

function updatePageWithResponse(food, responseText) {
    var response_json = JSON.parse(responseText);
    var time = parseFloat(response_json['time']);

    if(time == null) {
        console.log('Unknown food');
    } else {
        sessionStorage.setItem("responseText", responseText);
        sessionStorage.setItem("food", food);
        window.open('../public/result.html', "_self");
    }
}
