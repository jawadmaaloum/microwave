import unittest
import requests
import json
import random
from reviews import *

class TestServer(unittest.TestCase):

    SITE_URL    = "http://localhost:51048/microwave/" # HOST:PORT
    REVIEWS_URL = SITE_URL + "reviews"

    print("Testing for server: " + SITE_URL)

    # Helper functions
    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def undo_review(self, food, review):
        rev_data = open_reviews()
        rev_data[food][review] -= 1
        close_reviews(rev_data)

    # Test set-up
    def setUp(self):
        with open('../data/times.json', 'r') as f:
            self.data = json.loads(f.read())

    # Test Functions
    def test_get_time_key(self):
        food = random.choice(list(self.data['food']))
        start_temp = random.choice(list(self.data['temp']))
        wattage = random.choice(list(self.data['wattage_ratios']))

        resp = requests.get(self.SITE_URL + food + "/" + start_temp + "/" + wattage)
        self.assertTrue(self.is_json(resp.content.decode()))

        resp_json = json.loads(resp.content.decode())
        self.assertTrue(resp_json['result'] == 'success')
        self.assertTrue(resp_json['time'] == self.data['food'][food] *
        self.data['wattage_ratios'][wattage] * self.data['temp'][start_temp])

    def test_get_review_all(self):
        resp = requests.get(self.REVIEWS_URL)
        rev_data = open_reviews()
        self.assertTrue(self.is_json(resp.content.decode()))

        resp_json = json.loads(resp.content.decode())
        self.assertTrue(resp_json['result'] == 'success')
        self.assertTrue(resp_json['reviews'] == [{food: {'good':
            rev_data[food]['good'], 'bad': rev_data[food]['bad']}} for food in
            rev_data.keys()])

    def test_get_review_key(self):
        rev_data = open_reviews()
        food = random.choice(list(rev_data.keys()))
        resp = requests.get(self.REVIEWS_URL + '/' + food)
        self.assertTrue(self.is_json(resp.content.decode()))

        resp_json = json.loads(resp.content.decode())
        self.assertTrue(resp_json['result'] == 'success')
        self.assertTrue(resp_json['good'] == rev_data[food]['good'])
        self.assertTrue(resp_json['bad'] == rev_data[food]['bad'])

    def test_post_review_index(self):
        rev_data = open_reviews()
        food = random.choice(list(rev_data.keys()))
        review = random.choice(["good", "bad"])
        body = {'food': food, 'review': review}

        resp = requests.post(self.REVIEWS_URL, json.dumps(body))
        self.assertTrue(self.is_json(resp.content.decode()))

        resp_json = json.loads(resp.content.decode())
        self.assertTrue(resp_json['result'] == 'success')

        rev_data = open_reviews()
        resp_get = requests.get(self.REVIEWS_URL + '/' + food)
        self.assertTrue(self.is_json(resp_get.content.decode()))
        get_json = json.loads(resp_get.content.decode())

        self.assertTrue(get_json[review] == rev_data[food][review])

        self.undo_review(food, review)

if __name__ == "__main__":
    unittest.main()
