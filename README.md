# Microwave

A website that tells you how long to microwave your food.

## Server API

| Request Type | Resource Endpoint | Body | Response | Description |
| --- | --- | --- | --- | --- |
| GET | /microwave/:food/:start_temp/:wattage | N/A | \{"result": "success", "time": x\} | Get time for specific food |
| GET | /microwave/reviews | N/A | \{"result": success, "reviews": \[\{"pasta": \{"good": x, "bad": y\}\}, ….\]\} | Get list of reviews |
| GET | /microwave/reviews/:food | N/A | \{"result": "success", "good": x, "bad": y\} | Get reviews for specific food |
| POST | /microwave/reviews | \{"food": "pasta", "review": "good"\} | \{"result": "success"} | Post a review |
## Testing

##### API

Test the server and API by running `python3 src/test_api.py`  
This will run unit tests for each of the different API calls

## UI  
*Index.html*
![image](https://user-images.githubusercontent.com/10440773/116830183-58d86780-ab76-11eb-9b65-a42af8e62e6d.png)  

*Result.html*
![image](https://user-images.githubusercontent.com/10440773/116830187-70175500-ab76-11eb-9d91-ca1c05f296a3.png)


## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.
